## SIMPLE CHAT APP

Make sure you have node.js installed.

1. cd into `project_root_folder`
2. run `npm install` to install dependencies.
3. run `node install.js` to install app and migrate database
4. run `npm start` OR `node index.js` to start chat server.