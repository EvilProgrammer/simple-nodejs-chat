class Base64 {
    static encode(str) {
        if (typeof str === 'object') {
            str = JSON.stringify(str);
        }

        return btoa(str);
    }

    static decode(str) {
        return atob(str);
    }

    static decodeToJson(str) {
        return JSON.parse(Base64.decode(str));
    }
}

function hideKeyboard() {
    //this set timeout needed for case when hideKeyborad
    //is called inside of 'onfocus' event handler
    setTimeout(function () {

        //creating temp field
        var field = document.createElement('input');
        field.setAttribute('type', 'text');
        //hiding temp field from peoples eyes
        //-webkit-user-modify is nessesary for Android 4.x
        field.setAttribute('style', 'position:absolute; top: 0px; opacity: 0; -webkit-user-modify: read-write-plaintext-only; left:0px;');
        document.body.appendChild(field);

        //adding onfocus event handler for out temp field
        field.onfocus = () => {
            //this timeout of 200ms is nessasary for Android 2.3.x
            setTimeout(() => {

                field.setAttribute('style', 'display:none;');
                setTimeout(function () {
                    document.body.removeChild(field);
                    document.body.focus();
                }, 14);

            }, 200);
        };
        //focusing it
        field.focus();

    }, 50);
}


const socket = io();

new Vue({
    data: {
        username: null,
        login: {
            username: '',
            password: ''
        },
        form: {
            message: ''
        },

        myMessageClass: 'my-chat ml-5 rounded mb-3 text-white',
        replyMessageClass: 'reply-chat mr-5 rounded mb-3 text-white',

        messages: [],

        isLogged: false,
        isLoading: false,
        showLoginError: false,
    },

    created() {
        let user = sessionStorage.getItem('loggedInUser');
        if (user !== null) {
            this.returnFromLogin(user);
        }

        socket.on('returnFromLogin', ($logged) => {
            this.returnFromLogin($logged)
        });

        socket.on('returnFromRegister', ($value) => {
            this.returnFromRegister($value)
        });

        socket.on('returnMessages', ($messages) => {
            this.returnMessages($messages)
        });

        socket.on('returnMessage', ($message) => {
            this.returnMessage($message)
        });
    },

    methods: {
        onFormSubmit() {
            const message = this.form.message;
            let data = {
                username: this.username,
                message
            };
            this.returnMessage(data);
            socket.emit('sendMessage', data);
            this.form.message = '';
            hideKeyboard();
        },

        logout() {
            sessionStorage.removeItem('loggedInUser');
            this.messages = [];
            this.username = null;
            window.location.reload();
        },

        loginUser() {
            this.isLoading = true;
            this.showLoginError = false;
            socket.emit('loginUser', Base64.encode(this.login));
        },

        registerUser() {
            this.isLoading = true;
            this.showLoginError = false;
            socket.emit('registerUser', Base64.encode(this.login));
        },

        returnMessage(message) {
            this.messages.push(message);
            this.scrollToBottom();
        },

        scrollToBottom() {
            setTimeout(function () {
                const el = document.getElementById('messages-panel');
                if (el !== null) {
                    el.scrollTop = 2000;
                }
            }, 200);

        },

        returnMessages(messages) {
            this.messages = messages;
            this.scrollToBottom();
        },

        returnFromLogin($logged) {
            this.isLoading = false;
            if ($logged !== false) {
                sessionStorage.setItem('loggedInUser', $logged);
                this.username = Base64.decode($logged);
                this.isLogged = true;
                socket.emit('getMessages', this.username);
            }

            this.showLoginError = true;
        },

        returnFromRegister($value) {
            this.isLoading = false;
            if ($value === false) {
                alert("Username: " + this.login.username + " has been taken!")
            }
        }
    }
}).$mount('#appEngine');
