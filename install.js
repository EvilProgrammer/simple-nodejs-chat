const fs = require('fs');

const sourceFile = './core/db/app-example.sqlite';
const mainFile = './core/db/app.sqlite';

fs.copyFile(sourceFile, mainFile, function (err) {
    if (err) throw err;
    if(fs.existsSync(mainFile)){
        console.log('Installation successful!')
    }else{
        console.log('Could not copy to app.sqlite')
    }
});
