// Require .env
require('dotenv').config();

// Require file system to check if database exists
const fs = require('fs');
// Database File
const databaseFile = './core/db/app.sqlite';
// Check if database file exits.
if (!fs.existsSync(databaseFile)) {
    console.log('Database file not found.');
    console.log('Run: node install.js to install.');
    // Stop running code;
    process.exit();
}

// Import Needed Scripts
let express = require('express');
let app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);
const sqlite = require('sqlite3');

// TrapCodeJs Base64 class for nodeJs
class Base64 {
    static encode(str) {
        if (typeof str === 'object') {
            str = JSON.stringify(str);
        }

        return Buffer.from(str).toString('base64');
    }

    static decode(str) {
        return Buffer.from(str, 'base64').toString('ascii');
    }

    static decodeToJson(str) {
        str = Base64.decode(str);
        return JSON.parse(str);
    }
}

// Declare public files folder
app.use(express.static('static'));
// Set routes.
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

// Start Server
http.listen(process.env.PORT, function () {
    console.log('Sever started and available on http://localhost:' + process.env.PORT);
});


// Start Database Connection
let db = new sqlite.Database(databaseFile, function (err) {
    if (err) {
        console.log('Database connection failed.');
        process.exit();
    }
});

// Start Socket Connection
io.on('connection', function (socket) {
    // On loginUser
    socket.on('loginUser', function (form) {
        // Decode Form from base64 (Not the best option, use BCRYPT instead)
        form = Base64.decodeToJson(form);
        // Encode password
        form.password = Base64.encode(form.password);
        // Check Database for username and encoded password;
        db.get('SELECT * FROM users WHERE username = ? AND password = ?', [form.username, form.password], function (err, rows) {
            let hasResult = false;
            // if found results
            if (typeof rows !== "undefined") {
                // Return encoded username to user
                hasResult = Base64.encode(form.username);
            }
            socket.emit('returnFromLogin', hasResult)
        })

    });

    // On registerUser
    socket.on('registerUser', function (form) {
        form = Base64.decodeToJson(form);
        form.password = Base64.encode(form.password);
        db.get(`SELECT * FROM users WHERE username = ?`, [form.username], function (err, rows) {
            if (typeof rows === "undefined") {
                db.run(`INSERT INTO users (username, password) VALUES (?, ?)`, [form.username, form.password], function (err1, rows1) {
                    if (err1 === null) {
                        socket.emit('returnFromLogin', Base64.encode(form.username))
                    }
                })
            } else {
                socket.emit('returnFromRegister', false)
            }
        })

    });

    // On getMessages
    socket.on('getMessages', function (username) {
        db.all(`SELECT * FROM messages ORDER BY created_at ASC`, function (err, rows) {
            let hasResult = [];
            if (typeof rows !== "undefined") {
                hasResult = rows;
            }
            socket.emit('returnMessages', hasResult)
        })
    });

    // On sendMessage
    socket.on('sendMessage', function (data) {
        db.run(`INSERT INTO messages (username, message) VALUES (?, ?)`, [data.username, data.message]);
        socket.broadcast.emit('returnMessage', data);
    })
});
